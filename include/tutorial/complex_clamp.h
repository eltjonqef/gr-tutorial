/* -*- c++ -*- */
/*
 * gr-tutorial: Useful blocks for SDR and GNU Radio learning
 *
 *  Copyright (C) 2019, 2020 Manolis Surligas <surligas@gmail.com>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef INCLUDED_TUTORIAL_COMPLEX_CLAMP_H
#define INCLUDED_TUTORIAL_COMPLEX_CLAMP_H

#include <tutorial/api.h>
#include <gnuradio/sync_block.h>

namespace gr {
namespace tutorial {

/*!
 * \brief <+description of block+>
 * \ingroup tutorial
 *
 */
class TUTORIAL_API complex_clamp : virtual public gr::sync_block {
public:
    typedef boost::shared_ptr<complex_clamp> sptr;

    /*!
     * \brief Return a shared_ptr to a new instance of tutorial::complex_clamp.
     *
     * To avoid accidental use of raw pointers, tutorial::complex_clamp's
     * constructor is in a private implementation
     * class. tutorial::complex_clamp::make is the public interface for
     * creating new instances.
     */
    static sptr make(float threshold = 0.5);
};

} // namespace tutorial
} // namespace gr

#endif /* INCLUDED_TUTORIAL_COMPLEX_CLAMP_H */


/* -*- c++ -*- */
/*
 * gr-tutorial: Useful blocks for SDR and GNU Radio learning
 *
 *  Copyright (C) 2019, 2020 Manolis Surligas <surligas@gmail.com>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gnuradio/io_signature.h>
#include "frame_sync_impl.h"

namespace gr {
namespace tutorial {

frame_sync::sptr
frame_sync::make(uint8_t preamble, uint8_t preamble_len,
                 const std::vector<uint8_t> &sync_word,
                 int mod)
{
    return gnuradio::get_initial_sptr
           (new frame_sync_impl(preamble, preamble_len, sync_word, mod));
}


/*
 * The private constructor
 */
frame_sync_impl::frame_sync_impl(uint8_t preamble, uint8_t preamble_len,
                                 const std::vector<uint8_t> &sync_word,
                                 int mod)
    : gr::sync_block("frame_sync",
                     gr::io_signature::make(1, 1, sizeof(uint8_t)),
                     gr::io_signature::make(0, 0, 0)),
      d_mod((mod_t)mod),
      d_preamble(preamble),
      d_preamble_len(preamble_len << 3),
      d_sync_word(sync_word),
      d_sync_word_len(sync_word.size() << 3),
      d_state(PREAMBLE),
      d_byte(0),
      d_index(7)
{
    message_port_register_out(pmt::mp("pdu"));
}

/*
 * Our virtual destructor.
 */
frame_sync_impl::~frame_sync_impl()
{
}

int
frame_sync_impl::work(int noutput_items,
                      gr_vector_const_void_star &input_items,
                      gr_vector_void_star &output_items)
{
    const uint8_t *in = (const uint8_t *) input_items[0];
    // Do <+signal processing+>
    d_index = 0;
    while (d_index < noutput_items - 8) {
        decode(in, noutput_items);
    }
    /*
     * GNU Radio handles PMT messages in a pair structure.
     * The first element corresonds to possible metadata (in our case we do not
     * have assosiated metadata) whereas the second element contains the
     * data in a vector of uint8_t.
     *
     * When the frame is extracted, you will possible hold it in a *uint8_t buffer,
     * To create a PMT pair message you can use:
     * pmt::pmt_t pair = pmt::cons(pmt::PMT_NIL, pmt::init_u8vector(frame_len, buf));
     *
     * Then you can send this message to the port we have registered at the contructpr
     * using:
     *
     * message_port_pub(pmt::mp("pdu"), pair);
     */



    // Tell runtime system how many output items we produced.
    return noutput_items;
}

void
frame_sync_impl::decode(const uint8_t *in, int len)
{
    switch (d_state) {
    case PREAMBLE:
        search_preamble(in, len);
        break;
    case SYNC:
        search_sync_word(in, len);
        break;
    case LENGTH:
        get_pdu_length(in, len);
        break;
    case PAYLOAD:
        decode_payload(in, len);
        break;
    default:
        break;
    }
}

void
frame_sync_impl::search_preamble(const uint8_t *in, int len)
{

    for (; d_index < len - 8;) {
        d_byte = in[d_index];
        d_byte = (d_byte << 1) | in[d_index + 1];
        d_byte = (d_byte << 1) | in[d_index + 2];
        d_byte = (d_byte << 1) | in[d_index + 3];
        d_byte = (d_byte << 1) | in[d_index + 4];
        d_byte = (d_byte << 1) | in[d_index + 5];
        d_byte = (d_byte << 1) | in[d_index + 6];
        d_byte = (d_byte << 1) | in[d_index + 7];
        if (d_byte == d_preamble) {
            d_preamble_counter++;
            d_index += 8;
            if (d_preamble_counter == 10) {
                d_state = SYNC;
                d_preamble_counter = 0;
                return;
            }
            continue;
        }
        d_preamble_counter = 0;
        d_index++;
    }
}
/*
void
frame_sync_impl::search_preambleQPSK(const uint8_t *in, int len)
{

    for (; d_index < len - 8;) {
        d_byte = in[d_index];
        d_byte = (d_byte << 2) | in[d_index + 1];
        d_byte = (d_byte << 2) | in[d_index + 2];
        d_byte = (d_byte << 2) | in[d_index + 3];
        d_byte = (d_byte << 1) | in[d_index + 4];
        d_byte = (d_byte << 1) | in[d_index + 5];
        d_byte = (d_byte << 1) | in[d_index + 6];
        d_byte = (d_byte << 1) | in[d_index + 7];
        if (d_byte == d_preamble) {
            d_preamble_counter++;
            d_index += 8;
            if (d_preamble_counter == 10) {
                d_state = SYNC;
                d_preamble_counter = 0;
                return;
            }
            continue;
        }
        d_preamble_counter = 0;
        d_index++;
    }
}
*/
void
frame_sync_impl::search_sync_word(const uint8_t *in, int len)
{

    for (; d_index < len - 8;) {
        d_byte = in[d_index];
        d_byte = (d_byte << 1) | in[d_index + 1];
        d_byte = (d_byte << 1) | in[d_index + 2];
        d_byte = (d_byte << 1) | in[d_index + 3];
        d_byte = (d_byte << 1) | in[d_index + 4];
        d_byte = (d_byte << 1) | in[d_index + 5];
        d_byte = (d_byte << 1) | in[d_index + 6];
        d_byte = (d_byte << 1) | in[d_index + 7];
        if (d_byte == d_sync_word[d_sync_word_counter]) {
            d_sync_word_counter++;
            d_index += 8;
            if (d_sync_word_counter == d_sync_word.size()) {
                d_state = LENGTH;
                d_sync_word_counter = 0;
                return;
            }
            continue;
        }
        d_sync_word_counter = 0;
        d_index++;
        if (d_index > ((d_preamble_len + d_sync_word_len + 5) << 3)) {
            d_state = PREAMBLE;
            return;
        }
    }
}


void
frame_sync_impl::get_pdu_length(const uint8_t *in, int len)
{

    if (d_index < len - 16) {
        d_pdu_len = in[d_index++];
        d_pdu_len = (d_pdu_len << 1) | in[d_index++];
        d_pdu_len = (d_pdu_len << 1) | in[d_index++];
        d_pdu_len = (d_pdu_len << 1) | in[d_index++];
        d_pdu_len = (d_pdu_len << 1) | in[d_index++];
        d_pdu_len = (d_pdu_len << 1) | in[d_index++];
        d_pdu_len = (d_pdu_len << 1) | in[d_index++];
        d_pdu_len = (d_pdu_len << 1) | in[d_index++];
        d_pdu_len = (d_pdu_len << 1) | in[d_index++];
        d_pdu_len = (d_pdu_len << 1) | in[d_index++];
        d_pdu_len = (d_pdu_len << 1) | in[d_index++];
        d_pdu_len = (d_pdu_len << 1) | in[d_index++];
        d_pdu_len = (d_pdu_len << 1) | in[d_index++];
        d_pdu_len = (d_pdu_len << 1) | in[d_index++];
        d_pdu_len = (d_pdu_len << 1) | in[d_index++];
        d_pdu_len = (d_pdu_len << 1) | in[d_index++];
        d_state = PAYLOAD;
    }
    else {
        d_state = PREAMBLE;
    }
}

void
frame_sync_impl::decode_payload(const uint8_t *in, int len)
{

    int consume = len - d_index;
    if ((d_pdu_len << 3) - buffer.size() < consume) {
        consume = (d_pdu_len << 3) - buffer.size();
    }
    for (int i = 0; i < consume; i++) {
        buffer.push_back(in[d_index++]);
    }
    if (buffer.size() == (d_pdu_len << 3)) {
        uint8_t *payload = new uint8_t[d_pdu_len];
        for (int i = 0; i < (d_pdu_len << 3); i += 8) {
            d_byte = buffer[i];
            d_byte = (d_byte << 1) | buffer[i + 1];
            d_byte = (d_byte << 1) | buffer[i + 2];
            d_byte = (d_byte << 1) | buffer[i + 3];
            d_byte = (d_byte << 1) | buffer[i + 4];
            d_byte = (d_byte << 1) | buffer[i + 5];
            d_byte = (d_byte << 1) | buffer[i + 6];
            d_byte = (d_byte << 1) | buffer[i + 7];
            payload[i >> 3] = d_byte;
        }
        pmt::pmt_t pair = pmt::cons(pmt::PMT_NIL, pmt::init_u8vector(d_pdu_len,
                                    payload));
        message_port_pub(pmt::mp("pdu"), pair);
        buffer.clear();
        delete[] payload;
        d_state = PREAMBLE;
    }
}
} /* namespace tutorial */
} /* namespace gr */


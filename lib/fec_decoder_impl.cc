/* -*- c++ -*- */
/*
 * gr-tutorial: Useful blocks for SDR and GNU Radio learning
 *
 *  Copyright (C) 2019, 2020 Manolis Surligas <surligas@gmail.com>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gnuradio/io_signature.h>
#include "fec_decoder_impl.h"

namespace gr {
namespace tutorial {

fec_decoder::sptr
fec_decoder::make(int type)
{
    return gnuradio::get_initial_sptr
           (new fec_decoder_impl(type));
}


/*
 * The private constructor
 */
fec_decoder_impl::fec_decoder_impl(int type)
    : gr::block("fec_encoder",
                gr::io_signature::make(0, 0, 0),
                gr::io_signature::make(0, 0, 0)),
      d_type(type)
{
    message_port_register_in(pmt::mp("pdu_in"));
    message_port_register_out(pmt::mp("pdu_out"));

    /* Register the message handler. For every message received in the input
     * message port it will be called automatically.
     */
    set_msg_handler(pmt::mp("pdu_in"),
    [this](pmt::pmt_t msg) {
        this->fec_decoder_impl::decode(msg);
    });
}

/*
 * Our virtual destructor.
 */
fec_decoder_impl::~fec_decoder_impl()
{
}

void
fec_decoder_impl::decode(pmt::pmt_t m)
{
    switch (d_type) {
    /* No FEC just copy the input message to the output */
    case 0:
        message_port_pub(pmt::mp("pdu_out"), m);
        return;
    case 1: {
        /* Do Hamming encoding */
        pmt::pmt_t bytes(pmt::cdr(m));
        size_t pdu_len;
        const uint8_t *bytes_in = pmt::u8vector_elements(bytes, pdu_len);
        uint8_t *payload = new uint8_t[pdu_len / 3];
        uint8_t byte;
        int index = 0;
        int bit;
        for (int i = 0; i < (pdu_len << 3); i += 3) {
            bit = (bytes_in[i >> 3] >> (7 - (i % 8))) & 0x01;
            bit <<= 1;
            bit |= (bytes_in[(i + 1) >> 3] >> (7 - ((i + 1) % 8))) & 0x01;
            bit <<= 1;
            bit |= (bytes_in[(i + 2) >> 3] >> (7 - ((i + 2) % 8))) & 0x01;
            payload[index >> 3] = (payload[index >> 3] << 1) | map[bit];
            index++;
        }
        bytes = pmt::make_blob(payload, pdu_len / 3);
        message_port_pub(pmt::mp("pdu_out"), pmt::cons(pmt::PMT_NIL, bytes));
        delete[] payload;
        return;
    }
    case 2:
        /* Do Golay encoding */
        return;
    default:
        throw std::runtime_error("fec_decoder: Invalid FEC");
        return;
    }
}

} /* namespace tutorial */
} /* namespace gr */

